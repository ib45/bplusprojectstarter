import React from 'react';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import {createDrawerNavigator} from '@react-navigation/drawer';
import {HomeTabBar, HomeScreen, HomeDrawer} from '../scenes/home';
import {AppRoute} from './app-routes';
import {PersonIcon, HomeIcon, LogoutIcon} from '../assets/icon';
import {ProfileNavigator} from './profile.navigator';
import {AuthNavigator} from './auth.navigator';

const BottomTab = createBottomTabNavigator();
const Drawer = createDrawerNavigator();

const HomeBottomNavigator = () => (
  <BottomTab.Navigator tabBar={props => <HomeTabBar {...props} />}>
    <BottomTab.Screen
      name={AppRoute.HOME}
      component={HomeScreen}
      options={{title: 'Home', tabBarIcon: HomeIcon}}
    />
    <BottomTab.Screen
      name={AppRoute.PROFILE}
      component={ProfileNavigator}
      options={{title: 'Profile', tabBarIcon: PersonIcon}}
    />
  </BottomTab.Navigator>
);

export const HomeNavigator = () => (
  <Drawer.Navigator drawerContent={props => <HomeDrawer {...props} />}>
    <Drawer.Screen
      name={AppRoute.HOME}
      component={HomeBottomNavigator}
      options={{title: 'Home', drawerIcon: HomeIcon}}
    />
  </Drawer.Navigator>
);
