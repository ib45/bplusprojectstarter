import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import {AppRoute} from './app-routes';
import {AuthNavigator} from './auth.navigator';
import { HomeNavigator } from './home.navigator';

const Stack = createStackNavigator();

export const AppNavigator = props => (
  <Stack.Navigator {...props} headerMode="none">
    <Stack.Screen name={AppRoute.AUTH} component={AuthNavigator} />
    <Stack.Screen name={AppRoute.HOME} component={HomeNavigator} />
  </Stack.Navigator>
);
