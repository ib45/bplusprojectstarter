export const AppRoute = {
  AUTH: 'Auth',
  SIGN_IN: 'Sign In',
  SIGN_UP: 'Sign Up',
  SIGN_OUT: 'Sign Out',
  HOME: 'Home',
  TODO: 'Todo',
  TODO_IN_PROGRESS: 'Todo In Progress',
  TODO_DONE: 'Todo Done',
  PROFILE: 'Profile',
};
