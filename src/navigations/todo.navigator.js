import React from 'react';
import {createMaterialTopTabNavigator} from '@react-navigation/material-top-tabs';
import {AppRoute} from './app-routes';
import {GridIcon, DoneAllIcon} from '../assets/icon';
import {TodoTabBar} from '../scenes/todo';

const TopTab = createMaterialTopTabNavigator();

export const TodoNavigator = () => (
  <TopTab.Navigator tabBar={props => <TodoTabBar {...props} />}>
    <TopTab.Screen
      name={AppRoute.TODO_IN_PROGRESS}
      component={() => <></>}
      options={{title: 'IN PROGRESS', tabBarIcon: GridIcon}}
    />
    <TopTab.Screen
      name={AppRoute.TODO_DONE}
      component={() => <></>}
      options={{title: 'DONE', tabBarIcon: DoneAllIcon}}
    />
  </TopTab.Navigator>
);
