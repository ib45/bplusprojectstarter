import React from 'react';
import {StyleSheet} from 'react-native';
import {Formik} from 'formik';
import {Button, Layout, Text} from '@ui-kitten/components';
import {FormInput} from '../../components/form-input.component';
import {SignUpData, SignUpSchema} from '../../models/sign-up.model';
import {AppRoute} from '../../navigations/app-routes';

const SignUpForm = ({onSubmit}) => {
  const renderForm = ({handleChange, handleBlur, handleSubmit}) => {
    return (
      <React.Fragment>
        <FormInput
          id="email"
          style={styles.formControl}
          placeholder="Email"
          keyboardType="email-address"
        />
        <FormInput
          id="password"
          style={styles.formControl}
          placeholder="Password"
        />
        <FormInput
          id="username"
          style={styles.formControl}
          placeholder="Username"
        />
        <Button
          appearance="outline"
          status="primary"
          style={styles.submitButton}
          onPress={handleSubmit}>
          SIGN UP
        </Button>
      </React.Fragment>
    );
  };

  return (
    <Formik
      initialValues={SignUpData}
      validationSchema={SignUpSchema}
      onSubmit={onSubmit}>
      {renderForm}
    </Formik>
  );
};

export const SignUpScreen = props => {
  const navigateSignIn = () => {
    props.navigation.navigate(AppRoute.SIGN_IN);
  };
  const navigateHome = () => {
    props.navigation.navigate(AppRoute.HOME);
  };

  const onFormSubmit = values => {
    navigateHome();
  };

  return (
    <Layout style={styles.formContainer}>
      <Text style={styles.formTitle} category="h1">
        Sign-Up Screen
      </Text>
      <SignUpForm onSubmit={onFormSubmit} />
      <Button
        style={styles.haveAccountButton}
        appearance="ghost"
        status="basic"
        onPress={navigateSignIn}>
        Already have an account?
      </Button>
    </Layout>
  );
};

const styles = StyleSheet.create({
  formContainer: {
    flex: 1,
    paddingVertical: 16,
    paddingHorizontal: 16,
  },
  formTitle: {
    margin: 8,
  },
  formControl: {
    marginVertical: 4,
  },
  submitButton: {
    marginVertical: 24,
  },
  haveAccountButton: {
    alignSelf: 'center',
  },
});
