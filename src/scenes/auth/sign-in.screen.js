import React, {useState} from 'react';
import {View, StyleSheet} from 'react-native';
import {Formik} from 'formik';
import {Layout, Text, Button} from '@ui-kitten/components';
import {FormInput} from '../../components/form-input.component';
import {EyeIcon, EyeOffIcon} from '../../assets/icon';
import {SignInSchema, SignInData} from '../../models/sign-in.model';
import {AppRoute} from '../../navigations/app-routes';

const SignInForm = ({onSubmit}) => {
  const [passwordVisible, setPasswordVisible] = useState(false);

  const onPasswordIconPress = () => {
    setPasswordVisible(!passwordVisible);
  };

  const renderForm = ({handleChange, handleBlur, handleSubmit}) => {
    return (
      <React.Fragment>
        <FormInput
          id="email"
          style={styles.formControl}
          placeholder="Email"
          keyboardType="email-address"
        />
        <FormInput
          id="password"
          style={styles.formControl}
          placeholder="Password"
          secureTextEntry={!passwordVisible}
          icon={passwordVisible ? EyeIcon : EyeOffIcon}
          onIconPress={onPasswordIconPress}
        />
        <Button
          appearance="outline"
          status="primary"
          title={'Sign In'}
          style={styles.submitButton}
          onPress={handleSubmit}>
          SIGN IN
        </Button>
      </React.Fragment>
    );
  };

  return (
    <Formik
      initialValues={SignInData}
      //validationSchema={SignInSchema}
      onSubmit={onSubmit}>
      {renderForm}
    </Formik>
  );
};

export const SignInScreen = props => {
  const navigateHome = () => {
    props.navigation.navigate(AppRoute.HOME);
  };
  const navigateSignUp = () => {
    props.navigation.navigate(AppRoute.SIGN_UP);
  };

  return (
    <Layout style={styles.formContainer}>
      <Text style={styles.formTitle} category="h1">
        Sign-In Screen
      </Text>
      <SignInForm onSubmit={navigateHome} />
      <Button
        title={'Sign Up'}
        style={styles.noAccountButton}
        appearance="ghost"
        status="basic"
        onPress={navigateSignUp}>
        Don't have an account?
      </Button>
    </Layout>
  );
};

const styles = StyleSheet.create({
  formContainer: {
    flex: 1,
    paddingVertical: 16,
    paddingHorizontal: 16,
  },
  formTitle: {
    margin: 8,
  },
  formControl: {
    marginVertical: 4,
  },
  submitButton: {
    marginVertical: 24,
  },
  noAccountButton: {
    alignSelf: 'center',
  },
});
