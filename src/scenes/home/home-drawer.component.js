import React from 'react';
import {Drawer, Menu, Text, Icon, Button} from '@ui-kitten/components';
import {
  SafeAreaLayout,
  SaveAreaInset,
} from '../../components/safearea-layout.component';
import {LogoutIcon} from '../../assets/icon';
import {AppRoute} from '../../navigations/app-routes';

export const HomeDrawer = props => {
  const onMenuItemSelect = index => {
    if (index !== 1) {
      const selectedTabRoute = props.state.routeNames[index];
      props.navigation.navigate(selectedTabRoute);
    } else {
      props.navigation.navigate(AppRoute.SIGN_IN);
    }
    props.navigation.closeDrawer();
  };

  const createNavigationItemForRoute = route => {
    const {options} = props.descriptors[route.key];
    return {
      routeName: route.name,
      title: <Text>{options.title}</Text>,
      icon: options.drawerIcon,
    };
  };

  const listMenu = [
    ...props.state.routes.map(createNavigationItemForRoute),
    {
      routeName: AppRoute.SIGN_OUT,
      title: <Text style={{color: 'red'}}>Logout</Text>,
      icon: style => LogoutIcon({...style, tintColor: 'red'}),
    },
  ];
  return (
    <SafeAreaLayout insets={SaveAreaInset.TOP}>
      <Drawer data={listMenu} onSelect={onMenuItemSelect} />
    </SafeAreaLayout>
  );
};
