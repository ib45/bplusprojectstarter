import React from 'react';
import {StyleSheet} from 'react-native';
import {Divider, Layout, Text} from '@ui-kitten/components';
import {Toolbar} from '../../components/toolbar.component';
import {
  SafeAreaLayout,
  SaveAreaInset,
} from '../../components/safearea-layout.component';
import {MenuIcon} from '../../assets/icon';

export const HomeScreen = props => (
  <SafeAreaLayout style={styles.safeArea} insets={SaveAreaInset.TOP}>
    <Toolbar
      title="React Navigation Ex 🐱"
      backIcon={MenuIcon}
      onBackPress={(props.navigation.toggleDrawer)}
    />
    <Divider />
    <Layout style={styles.container}>
      <Text category="h1">HOME</Text>
    </Layout>
  </SafeAreaLayout>
);

const styles = StyleSheet.create({
  safeArea: {
    flex: 1,
  },
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
});
