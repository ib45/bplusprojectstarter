import * as Yup from 'yup';

export const SignUpData = {
  email: '',
  username: '',
  password: '',
};
export const SignUpSchema = Yup.object().shape({
  email: Yup.string().email('Invaild email').required(),
  username: Yup.string().min(4, 'Username must be at least 4 characters').required(),
  password: Yup.string().min(8, 'Password must be at least 8 characters').required(),
});
