import React from 'react';
import {
  ApplicationProvider,
  Layout,
  Text,
  withStyles,
  IconRegistry,
} from '@ui-kitten/components';
import {mapping, light as lightTheme} from '@eva-design/eva';
import {default as appTheme} from './styles/custom-theme.json';
import {EvaIconsPack} from '@ui-kitten/eva-icons';
import {NavigationContainer} from '@react-navigation/native';
import {AppNavigator} from './navigations/app.navigator.js';
import {AppRoute} from './navigations/app-routes.js';

const theme = {...lightTheme, ...appTheme};
const isAuthorized = false;

const App = () => {
  return (
    <React.Fragment>
      <IconRegistry icons={EvaIconsPack} />
      <ApplicationProvider mapping={mapping} theme={theme}>
        <NavigationContainer>
          <AppNavigator
            initialRouteName={isAuthorized ? AppRoute.HOME : AppRoute.AUTH}
          />
        </NavigationContainer>
      </ApplicationProvider>
    </React.Fragment>
  );
};
export default App;
